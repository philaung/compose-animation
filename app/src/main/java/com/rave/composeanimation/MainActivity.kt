package com.rave.composeanimation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rave.composeanimation.components.Instagram
import com.rave.composeanimation.ui.theme.Blue
import com.rave.composeanimation.ui.theme.ComposeAnimationTheme
import com.rave.composeanimation.ui.theme.Cream
import com.rave.composeanimation.ui.theme.DarkGreen

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeAnimationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Blue
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                    ) {
                        var visibility by remember { mutableStateOf(false) }
                        var toggle by remember { mutableStateOf(false) }
                        Button(
                            onClick = { visibility = !visibility },
                            colors = ButtonDefaults.buttonColors(backgroundColor = Cream)
                        ) {
                            val showHide = if (visibility) "HIDE" else "SHOW"
                            Text(
                                text = "$showHide icon",
                                modifier = Modifier.padding(horizontal = 20.dp),
                                color = DarkGreen,
                                fontSize = 30.sp,
                                fontWeight = FontWeight.Bold
                            )
                        }
                        val transition = updateTransition(targetState = visibility, label = "")
                        val height by transition.animateValue(
                            typeConverter = Dp.VectorConverter,
                            transitionSpec = {
                                tween(durationMillis = 1000, easing = LinearEasing)
                            }, label = ""
                        ) {
                            when (it) {
                                true -> 300.dp
                                false -> 0.dp
                            }
                        }
                        AnimatedVisibility(
                            visible = visibility,
                            enter = scaleIn() + slideInVertically(tween(1000, easing = LinearEasing)),
                            exit = shrinkVertically(tween(1000, easing = LinearEasing))
                        ) {
                            IconButton(
                                onClick = { toggle = !toggle },
                                Modifier.height(height)
                            ) {
                                Instagram(200, toggle)
                                Instructions(toggle)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Instructions(iconToggle: Boolean) {
    Crossfade(
        targetState = iconToggle,
        animationSpec = tween(1500)
    ) { toggle ->
        if (toggle) Instruction("click icon to stop animation")
        else Instruction("click icon for a surprise")
    }
}

@Composable
fun Instruction(text: String) {
    Text(
        text,
        color = DarkGreen,
        fontSize = 25.sp,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(top = 250.dp)
    )
}
