/**
 * Created by Jimmy McBride on 2022-12-05
 *
 * Copyright © 2022 Jimmy McBride
 */
package com.rave.composeanimation.components

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

/**
 * Instagram
 *
 * @author Phil Aung on 2022-12-05.
 */
@Composable
fun Instagram(
    size: Int,
    toggle: Boolean,
) {
    var firstColor = Color.Red
    var secondColor = Color.Magenta
    var thirdColor = Color.Yellow

    if (toggle) {
        firstColor = getColor(Color.Red, listOf(Color.Yellow, Color.Magenta))
        secondColor = getColor(Color.Magenta, listOf(Color.Red, Color.Yellow))
        thirdColor = getColor(Color.Yellow, listOf(Color.Magenta, Color.Red))
    }

    val instagramColors = listOf(
        firstColor,
        secondColor,
        thirdColor
    )

    Canvas(modifier = Modifier
        .size(size.dp)
        .padding(15.dp),
        onDraw = {
            drawRoundRect(
                brush = Brush.linearGradient(colors = instagramColors),
                cornerRadius = CornerRadius(40f, 40f),
                style = Stroke(width = 30f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 50f,
                style = Stroke(width = 25f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(colors = instagramColors),
                radius = 15f,
                center = Offset(this.size.width * .80f, this.size.height * 0.20f),
            )
        }
    )
}

@Composable
fun getColor(color: Color, colorsBetween: List<Color>): Color {
    val duration = 3000
    val infiniteTransition = rememberInfiniteTransition()
    return infiniteTransition.animateColor(
        initialValue = color,
        targetValue = color,
        animationSpec = infiniteRepeatable(
            animation = keyframes {
                durationMillis = duration
                color at 0 with LinearEasing
                colorsBetween[0] at duration/3 with LinearEasing
                colorsBetween[1] at duration*2/3 with LinearEasing
                color at duration with LinearEasing
            }
        )
    ).value
}
